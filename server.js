var http = require('http');
var url = require('url');
/*
 - Simples (Padrão Bascio)
 
http.createServer(function (request, response) {
	response.writeHead(200, { "Content-Type": "text/plain" });
	response.write("Olá Mundo");
	response.end();
}).listen(8888);	
*/

/*
 - Simples Refatorado
function onRequest(request,response){
	console.log("Requisição recebida.");
	response.writeHead(200, { "Content-Type": "text/plain" });
	response.write("Olá Mundo");
	response.end();
}
http.createServer(onRequest).listen(8888);
console.log("Servidor iniciado.");
*/

/*
Simples Transformando em modulo 
function start() {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Requisição recebida." + pathname + ".");
		response.writeHead(200, { "Content-Type": "text/plain" });
		response.write("Olá Mundo");
		response.end();
	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;
*/

/*
 Simpres usando rotas em modulo
function start(route) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Requisição recebida." + pathname + ".");
		route(pathname);
		
		response.writeHead(200, { "Content-Type": "text/plain" });
		response.write("Olá Mundo");
		response.end();
	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;

*/

/*
 Simples usando injeção de dependencia

function start(route, handle) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Requisição recebida." + pathname + ".");
		route(handle, pathname);

		response.writeHead(200, { "Content-Type": "text/plain" });
		response.write("Olá Mundo");
		response.end();
	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;
*/

/*
 Simples pegando valores da rota e colocando na pagina automaticamente
function start(route, handle) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Requisição recebida." + pathname + ".");
		
		response.writeHead(200, { "Content-Type": "text/plain" });
		var content = route(handle, pathname);
		response.write(content);
		response.end();
	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;
*/
/*
 Simples operações não bloqueantes
function start(route, handle) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Requisição recebida." + pathname + ".");
		route(handle, pathname, response);
	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;
*/

/* 
trabalhando com o post, recebendo conteudo por partes 
function start(route, handle) {
	function onRequest(request, response) {
		var postData = "";
		var pathname = url.parse(request.url).pathname;
		console.log("Requisição recebida." + pathname + ".");

		request.setEncoding("utf8");

		request.addListener("data", function (postDataChunk) {
			postData += postDataChunk;
			console.log("Recebido parte dos dados do POST '" +
				postDataChunk + "'.");
		});
		request.addListener("end", function () {
			route(handle, pathname, response,postData);
		});

	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;
*/
function start(route, handle) {
	function onRequest(request, response) {

		var pathname = url.parse(request.url).pathname;
		console.log("Recebida requisição para" + pathname + ".");
		route(handle, pathname, response, request);

	}
	http.createServer(onRequest).listen(8888);
	console.log("Servidor iniciado.");
}
exports.start = start;