//var exec = require("child_process").exec;
var querystring = require("querystring"),
    fs = require("fs"),
    formidable = require("formidable");

function start(response, postData) {
    console.log("Manipulador de requisição 'start' foi invocado");

    var body = '<html>' +
        '<head>' +
        '<meta http-equiv="Content-Type" content="text/html; ' +
        'charset=UTF-8" />' +
        '</head>' +
        '<body>' +
        '<form action="/upload" enctype="multipart/form-data" method="post">' +
        //'<textarea name="text" rows="20" cols="60"></textarea>' +
        '<input type="file" name="upload" nultiple="multiple">' +
        '<input type="submit" value="Enviar arquivo" />' +
        '</form>' +
        '</body>' +
        '</html>';
    response.writeHead(200, { "Content-Type": "text/html" });
    response.write(body);
    response.end();

    /*
    Teste de callback para bloqueante
    exec("ls -lah", function (error, stdout, stderr) {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.write(stdout);
        response.end();
    });
    */
    /*
    Teste tempo de espera bloqueante 
    exec("find /",
        { timeout: 10000, maxBuffer: 20000 * 1024 },
        function (error, stdout, stderr) {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.write(stdout);
            response.end();
        });
     */
}

function upload(response, request) {
    console.log("Manipulador de requisição 'upload' foi invocado.");
    var form = new formidable.IncomingForm();
    console.log("preste a analisar");
    form.parse(request, function (error, fields, files) {
        console.log("análise finalizada");
        /* Possível erro em sistemas Windows:
        tentativa usar nome de um arquivo já existente */
        fs.rename(files.upload.path, "./tmp/test.png", function (error) {
            if (error) {
                fs.unlink("./tmp/test.png");
                fs.rename(files.upload.path, "./tmp/test.png");
            }
        });
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write("imagem recebida:<br/>");
        response.write("<img src='/show' />");
        response.end();
    });
}

function show(response) {
    console.log("Manipulador de requisição 'show' foi invocado");
    console.log("Manipulador de requisição 'show' foi invocado");
    response.writeHead(200, { "Content-Type": "image/png" });
    fs.createReadStream("./tmp/test.png").pipe(response);
}

exports.start = start;
exports.upload = upload;
exports.show = show;